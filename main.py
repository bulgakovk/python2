import cv2

import cv2

src   = cv2.imread("text.jpg")
gauss   = cv2.GaussianBlur(src, (3, 3), 0)
laplace = cv2.Laplacian(gauss, cv2.CV_64F)
result  = cv2.convertScaleAbs(laplace)

cv2.imwrite("result.bmp", result)


